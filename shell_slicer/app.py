"""
半径の切り方と適切なプロファイルを作成する
"""
import sys
import json
import numpy as np
from astropy import units as u
from astropy.table import Table
import pathlib


class ShellSlicer:
    """
    球殻の切り方を決める。
    log と lin が使える。lin は 0 から開始できるが、log は 0 から始められない。
    """

    def __init__(self, start_radius_arcmin, max_radius_arcmin, nr_of_shells):
        self._start_radius_arcmin = start_radius_arcmin
        self._max_radius_arcmin = max_radius_arcmin
        self._nr_of_shells = nr_of_shells

    def slice_by(self, method):
        """
        method は 'log' or 'lin'
        """
        if method == 'log':
            return np.geomspace(
                self._start_radius_arcmin, self._max_radius_arcmin,
                self._nr_of_shells) * u.arcmin
        elif method == 'lin':
            return np.linspace(
                self._start_radius_arcmin, self._max_radius_arcmin,
                self._nr_of_shells) * u.arcmin
        elif type(method) is str:
            return ValueError
        else:
            return TypeError


class CelestialObject:
    """天体"""

    def __init__(self, name, parameterization):
        self.name = name
        self._parameterization = parameterization

    def parameterize(self, quantity, radius):
        return {'method': self._parameterization.by,
                'quantities': self._parameterization.calculate(quantity, radius)}

    def parameterize_all_quantity(self, radius):
        return {'method': self._parameterization.by,
                'quantities': self._parameterization.calculate_all_quantity(radius)}


class CelestialObjectShellGeometry:
    """天体の球殻ジオメトリを Table 化する"""

    def __init__(self, celestial_object, shells):
        self._obj = celestial_object
        self.name = self._obj.name
        self._shell_list = shells
        self._number_of_shells = len(shells)

    def create_table(self):
        result = self._obj.parameterize_all_quantity(self._shell_list)
        method_name = result['method']
        quantities = result['quantities']
        quantity_names = quantities.keys()
        quantity_values = quantities.values()
        return Table(
            data=[self._shell_list, ] + list(quantity_values),
            names=['shell_outer_radius', ] + list(quantity_names),
            meta={'method name': method_name})

    def _table_to_dict(self, table):
        quantity = dict()
        for colname, col in table.columns.items():
            quantity[colname] = {
                'unit': "{0}".format(col.unit),
                'rows': [row for row in col]
            }
        return {'objective_name': self.name,
                'parameterize_method': table.meta['method name'],
                'number_of_shells': self._number_of_shells,
                'table': quantity}

    def to_json(self, table):
        return json.dumps(self._table_to_dict(table))

    def to_json_file(self, table, filepath):
        with open(pathlib.Path(filepath), 'w') as f:
            json.dump(self._table_to_dict(table), f, indent=2)

    def create_table_to_json_file(self, filepath):
        self.to_json_file(self.create_table(), filepath)


class Parameterize:
    """
    CelestialObject に composite されることを意図している。
    インスタンスごとに独自にパラメタライズしたデータを返す。
    パラメタライズは関数によるものでもよいし、テーブルとして保持するのもあり。
    今のところ半径依存のパラメタライズしか想定していない。
    >>> Werner2009 = Parameterize(by = "Werner2009")
    >>> expr_ne = lambda radius: \
    ((1.2*0.1*(1 + (radius / (0.25 * u.arcmin))**2)**(-0.75)) * u.cm**-3)\
    .to(u.cm**-3).value
    >>> Werner2009.add_expression('ne', expr_ne)
    >>> NGC4636 = CelestialObject("NGC4636",[Werner2009,])
    >>> NGC4636.parameterize(quantity = 'ne', radius = 50 * u.arcmin)
    [4.242561139346507e-05]
    >>> from astropy.table import Table
    >>> NGC4636.parameterize(\
    quantity = 'ne', radius = Table([[1, 4, 5] * u.arcmin],\
    names=('ne',))['ne'])
    [array([0.01433325, 0.00186953, 0.00133913])]
    """

    def __init__(self, by, expressions=None):
        self.by = by
        if expressions is None:
            expressions = {}
        self._expression_map = expressions

    def add_expression(self, quantity, expression):
        self._expression_map[quantity] = expression

    def calculate(self, quantity, radius):
        return {quantity: self._expression_map[quantity](radius)}

    def calculate_all_quantity(self, radius):
        return {quantity: expr(radius)
                for quantity, expr in self._expression_map.items()}


if __name__ == '__main__':
    Werner2009 = Parameterize(
        "Werner2009",
        {'ne': lambda radius: ((1.2*0.1*(1 + (radius / (0.25 * u.arcmin))**2)**(-0.75)) * u.cm**-3).to(u.cm**-3),
         'kT': lambda radius: ((0.56 * (1 + 1.4 * (radius / (1.2 * u.arcmin))**4) / (1 + (radius / (1.2 * u.arcmin))**4)) * u.keV).to(u.keV),
         'metal_abund': lambda radius: (0.95 * (2 + (radius / (0.8 * u.arcmin))**3) / (1 + (radius / (0.8 * u.arcmin))**3) - 0.4)
         }
    )
    NGC4636 = CelestialObject("NGC4636", Werner2009)
    geom_NGC4636 = CelestialObjectShellGeometry(
        NGC4636, ShellSlicer(0.1, 5, 10).slice_by('log'))
    geom_NGC4636.create_table_to_json_file('geometry.json')
