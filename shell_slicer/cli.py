def main():
    import sys
    from .app import ShellSlicer
    if len(sys.argv) == 3:
        physical_volume = {'radius': int(sys.argv[1]),
                           'nr_of_shells': int(sys.argv[2])}
    else:
        print('Usage: $ python shell-slicer.py max_radius_arcmin nr_of_shells',
              file=sys.stderr)
        sys.exit(1)


if __name__ == '__main__':
    main()
