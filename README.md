# shell-slicer

## Installation

1. Clone this repository
2. `pip install .` in the cloned directory

## Sample

Run below,

```python
from shell_slicer import *
from astropy import units as u
import os

if __name__ == '__main__':
    Werner2009 = Parameterize(
        "Werner2009",
        {'ne': lambda radius: ((1.2*0.1*(1 + (radius / (0.25 * u.arcmin))**2)**(-0.75)) * u.cm**-3).to(u.cm**-3),
         'kT': lambda radius: ((0.56 * (1 + 1.4 * (radius / (1.2 * u.arcmin))**4) / (1 + (radius / (1.2 * u.arcmin))**4)) * u.keV).to(u.keV),
         'metal_abund': lambda radius: (0.95 * (2 + (radius / (0.8 * u.arcmin))**3) / (1 + (radius / (0.8 * u.arcmin))**3) - 0.4)
         }
    )
    NGC4636 = CelestialObject("NGC4636", Werner2009)
    geom_NGC4636 = CelestialObjectShellGeometry(
        NGC4636, ShellSlicer(0.1, 5, 10).slice_by('log'))
    os.makedirs('shell_geom', exist_ok=True)
    geom_NGC4636.create_table_to_json_file('shell_geom/geom.json')
```

and you get this.

```json
{
    "tables": [
        {
            "parameterize_method": "Werner2009",
            "table": {
                "shell_outer_radius": {
                    "unit": "arcmin",
                    "rows": [
                        0.1,
                        0.15444521049463789,
                        0.23853323044733007,
                        0.36840314986403866,
                        0.5689810202763907,
                        0.8787639344404101,
                        1.3572088082974532,
                        2.096144000826768,
                        3.2373940143476263,
                        5.000000000000001
                    ]
                },
                "ne": {
                    "unit": "1 / cm3",
                    "rows": [
                        0.10735882610211377,
                        0.09416336457772917,
                        0.07384875731244851,
                        0.05049277131908064,
                        0.030616026668908898,
                        0.017176444986968124,
                        0.009252366612292172,
                        0.004890564670068293,
                        0.0025636651949277668,
                        0.0013391307002675735
                    ]
                },
                "kT": {
                    "unit": "keV",
                    "rows": [
                        0.5600108019482086,
                        0.5600614471384483,
                        0.5603491733446748,
                        0.5619723114348101,
                        0.5707770543582552,
                        0.6100306956175501,
                        0.6990322816837418,
                        0.7622739632354205,
                        0.7798498019193268,
                        0.7832592792935312
                    ]
                },
                "metal_abund": {
                    "unit": "",
                    "rows": [
                        1.498148148148148,
                        1.4932132280428827,
                        1.4754677160922776,
                        1.4154804270462633,
                        1.2486482328016777,
                        0.958532001313881,
                        0.7114873837981407,
                        0.6000304526097421,
                        0.5641222022584649,
                        0.5538753266619924
                    ]
                }
            }
        },
        {
            "parameterize_method": "Foobar2018",
            "table": {
                "shell_outer_radius": {
                    "unit": "arcmin",
                    "rows": [
                        0.1,
                        0.15444521049463789,
                        0.23853323044733007,
                        0.36840314986403866,
                        0.5689810202763907,
                        0.8787639344404101,
                        1.3572088082974532,
                        2.096144000826768,
                        3.2373940143476263,
                        5.000000000000001
                    ]
                },
                "metal_abund": {
                    "unit": "",
                    "rows": [
                        0.0,
                        0.0,
                        0.0,
                        0.0,
                        0.0,
                        0.0,
                        0.0,
                        0.0,
                        0.0,
                        0.0
                    ]
                }
            }
        }
    ]
}
```
